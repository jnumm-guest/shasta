#!/usr/bin/make -f

# DH_VERBOSE := 1
export LC_ALL=C.UTF-8

include /usr/share/dpkg/default.mk

# for hardening you might like to uncomment this:
export DEB_BUILD_MAINT_OPTIONS=hardening=+all

DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)
MULTIARCH_SONAME = shasta.cpython-$(shell py3versions -vd | sed 's/\.//')-$(DEB_HOST_MULTIARCH).so

%:
	dh $@ --with python3,sphinxdoc

override_dh_auto_configure:
	dh_auto_configure -- -DBUILD_ID=${DEB_VERSION_UPSTREAM}

override_dh_auto_build: export http_proxy=127.0.0.1:9
override_dh_auto_build: export https_proxy=127.0.0.1:9
override_dh_auto_build:
	dh_auto_build
	# Move shared library into sphinx documentation folder for doc generation
	find . -name "shasta.so" -exec cp {} docs/PythonApi/ \;
	PYTHONPATH=docs/PythonApi/ python3 -m sphinx -N -bhtml docs/PythonApi/ build/html # HTML generator

override_dh_python3:
	dh_python3
	patchelf \
		--set-soname $(MULTIARCH_SONAME) \
		$(CURDIR)/debian/python3-shasta/usr/lib/python3/dist-packages/$(MULTIARCH_SONAME)

override_dh_install-arch:
	dh_install -a
	# Rename shastaDynamic to shasta for ease
	mv debian/shasta/usr/bin/shastaDynamic debian/shasta/usr/bin/shasta
	# The library is in a more unusual place (nested within /usr/lib), so modify
	# the ELF
	chrpath -r /usr/lib/python3/dist-packages \
		debian/shasta/usr/bin/shasta
	patchelf \
		--replace-needed shasta.so $(MULTIARCH_SONAME) \
		debian/shasta/usr/bin/shasta

override_dh_missing:
	# No need to install docker files
	find debian/tmp -type d -name docker | xargs rm -rf
	# static library should not be installed
	find debian/tmp -name shasta.a -delete
	# dynamic library was installed manually before - so remove it from debian/tmp
	find debian/tmp -name shasta.so -delete
	# 11MB of generated docs should go into a separate package if this is considered sensible
	# FIXME: for the moment do not install these
	find debian/tmp -name docs | xargs rm -rf
	# Remove rest of files which have already been installed
	rm -rf debian/tmp/${CURDIR}
	dh_missing --list-missing
